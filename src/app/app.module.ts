import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './material-components/material-components.module'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { ServersComponent } from './pages/servers/servers.component';
import { AdminComponent } from './pages/admin/admin.component';
import { PlayersComponent } from './pages/players/players.component';
import { TickAnimationComponent } from './tick-animation/tick-animation.component';
import { CrossAnimationComponent } from './cross-animation/cross-animation.component';
import { DataStoreService } from './services/data-store.service';

export function get_settings(dataStore: DataStoreService) {
    return () => dataStore.generateInitialSetup();
}

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    ServersComponent,
    AdminComponent,
    PlayersComponent,
    TickAnimationComponent,
    CrossAnimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DataStoreService,
    { provide: APP_INITIALIZER, useFactory: get_settings, deps: [DataStoreService], multi: true }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
