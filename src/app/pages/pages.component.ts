import { Component, OnInit } from '@angular/core';
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { DataStoreService } from '../services/data-store.service';
declare var electron: any;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  public selectedTab = "Servers";

  constructor(private dataStore: DataStoreService) { }

  ngOnInit() {
  }

  closeApp() {
    var appWindow = electron.remote.getCurrentWindow();
    appWindow.close();
  }

  maximizeApp() {
    var appWindow = electron.remote.getCurrentWindow();
       if (!appWindow.isMaximized()) {
           appWindow.maximize();
       } else {
           appWindow.unmaximize();
       }
  }

  minimizeApp() {
    var appWindow = electron.remote.getCurrentWindow();
       appWindow.minimize();
  }

  tabBackground(name) {
    if(this.selectedTab == name) {
      return "top-bar-action selected-tab mat-button";
    } else {
      return "top-bar-action mat-button";
    }
  }

}
