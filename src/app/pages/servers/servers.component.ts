import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs'
import { TickAnimationComponent } from '../../tick-animation/tick-animation.component';
import { CrossAnimationComponent } from '../../cross-animation/cross-animation.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataStoreService } from '../../services/data-store.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
declare var shell: any;

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
export class ServersComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  public queryPorts = [];
  public gamePorts = [];
  public gridExport;
  public seamlessPorts = [];
  public serverName: String;
  public fileReady: boolean = false;
  public portsConfirmed: boolean = false;
  public selectedPortType: String = "sgt";
  public displayedColumns: string[] = ['location', 'name', 'home', 'status', 'players'];
  private dataSource = new MatTableDataSource([{}]);
  public dataSet = false;

  @ViewChild('sectiononeTick') sectiononetick: TickAnimationComponent;
  @ViewChild('sectiononeCross') sectiononecross: CrossAnimationComponent;
  @ViewChild('sectiontwoTick') sectiontwotick: TickAnimationComponent;
  @ViewChild('sectiontwoCross') sectiontwocross: CrossAnimationComponent;
  @ViewChild('sectionthreeTick') sectionthreetick: TickAnimationComponent;
  @ViewChild('sectionthreeCross') sectionthreecross: CrossAnimationComponent;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('paginate') paginator: MatPaginator;

  constructor(private elementRef: ElementRef, private _formBuilder: FormBuilder, private dataStore: DataStoreService) {
    let that = this;
    that.dataStore.dataObservable.subscribe(data => {
      if(!that.isEmpty(data)) {
        that.gridExport = data["server_config"];
        that.dataSource = new MatTableDataSource(data["server_config"]["servers"]);
        that.dataSource.paginator = this.paginator;
        that.dataStore.dataSet = true
      }
    })
  }

  public isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

  public fileName() {
    let file = this.secondFormGroup.controls["jsonFile"].value;
    if (file) {
      var startIndex = (file.indexOf('\\') >= 0 ? file.lastIndexOf('\\') : file.lastIndexOf('/'));
      var filename = file.substring(startIndex);
      if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
          filename = filename.substring(1);
      }
      return filename
    }
  }

  public fileChange() {
    let reader = new FileReader();
    reader.onload = (e) => {this.onReaderLoad(e)};
    reader.readAsText(this.fileInput.nativeElement.files[0]);
  }

  public onReaderLoad(event) {
    var obj = JSON.parse(event.target.result);
    console.log(obj)
    this.gridExport = obj
    if (this.gridExport["totalGridsX"] && this.gridExport["totalGridsY"]) {
      this.sectiontwocross.erase()
      this.sectiononetick.draw()
      this.sectiontwotick.draw()
      this.fileReady = true;
    } else {
      this.sectiontwotick.erase()
      this.sectiontwocross.draw()
    }
    if (this.gridExport["WorldFriendlyName"]) {
      this.serverName = this.gridExport["WorldFriendlyName"]
    }
    if (this.gridExport["servers"]) {
      this.queryPorts = []
      this.gamePorts = []
      this.seamlessPorts = []
      let servers = this.gridExport["servers"]
      for (let i = 0; i < servers.length; i++) {
        this.queryPorts.push(servers[i]["port"])
        this.gamePorts.push(servers[i]["gamePort"])
        this.seamlessPorts.push(servers[i]["seamlessDataPort"])
      }
    }
  }

  drawThirdTick() {
    if (this.portsConfirmed) {
      this.sectionthreecross.erase();
      this.sectionthreetick.draw();
    } else {
      this.sectionthreetick.erase();
      this.sectionthreecross.draw();
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({});
    this.secondFormGroup = this._formBuilder.group({
      jsonFile: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      portType: ['sgt', Validators.required],
      queryPort: [''],
      gamePort: [''],
      seamlessPort: ['']
    });
  }

  downloadGridFile() {
    shell.openItem("https://github.com/GrapeshotGames/ServerGridEditor/archive/master.zip");
  }

  downloadWikiFile() {
    shell.openItem("https://github.com/GrapeshotGames/ServerGridEditor/wiki");
  }

  finaliseConfig() {
    this.dataStore.set("server_config", this.gridExport)
    this.dataStore.dataSet = true;
  }

  gridToNumber(num) {
    return String.fromCharCode(65 + num)
  }

  getHomeClass(element) {
    if (element.isHomeServer) {
      return "success-color mat-cell cdk-column-home mat-column-home ng-star-inserted"
    } else {
      return "error-color mat-cell cdk-column-home mat-column-home ng-star-inserted"
    }
  }

  getStatusClass(element) {
    if (element.innerHTML == " Offline ") {
      return "warn-color mat-cell cdk-column-status mat-column-status ng-star-inserted"
    } else if (element.innerHTML == " Starting "){
      return "info-color mat-cell cdk-column-status mat-column-status ng-star-inserted"
    } else if (element.innerHTML == " Running "){
      return "success-color mat-cell cdk-column-status mat-column-status ng-star-inserted"
    }
  }

}
