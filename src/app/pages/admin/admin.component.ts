import { Component, OnInit, Inject } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';

export interface DialogData {
  detail: string;
}

declare var electron: any;
declare var path: any;
declare var fs: any;
declare var __dirname: any;
declare var SteamCmd: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public steamcmdInstallStatus: boolean = false;
  private downloadStarted = false;
  public fileLocation;
  private fileLocationTextValue = new BehaviorSubject("C:\\AtlasServerHost")
  public fileLocationTextObservable = this.fileLocationTextValue.asObservable();
  public fileLocationText = "";
  public steamInstalled = "Not Installed";
  public steamcmd = new SteamCmd();
  public autoUpdateEnabled = false;

  constructor(private dataStore: DataStoreService, public dialog: MatDialog) {
    let that = this;
    this.dataStore.dataObservable.subscribe(value => {
      if (value && value["server_config"]["directory_location"]) {
        that.fileLocationTextValue.next(value["server_config"]["directory_location"])
      }
      that.fileLocationTextObservable.subscribe(directory => {
        that.steamcmd.setOptions({
          binDir: directory,
          installDir: directory + `/games`
        })
        that.steamcmdInstallStatus = false
        that.fileLocationText = directory;
        that.prepSteamInDirectory().then(() => {
          console.log("Here")
          that.checkSteamCMDInstall()
        })
      })
    })
  }

  autocheckChange(change) {
    this.autoUpdateEnabled = change.checked
  }

  async prepSteamInDirectory() {
    await this.steamcmd.prep()
  }

  ngOnInit() {
  }

  fileName(event) {
    this.fileLocationTextValue.next(event.files[0].path)
    this.updateLocation()
  }

  forceSteamCMDUpdate() {
    let that = this;
    that.prepSteamInDirectory().then(() => {
      that.checkSteamCMDInstall()
    })
  }

  updateLocation() {
    let update = this.dataStore.data["server_config"];
    update["directory_location"] = this.fileLocationText;
    this.dataStore.set("server_config", update);
  }

  checkSteamCMDInstall() {
    let location = this.fileLocationText + "\\steamcmd.exe";
    console.log(location)
    console.log(fs.existsSync(location))
    if(fs.existsSync(location)) {
      this.steamInstalled = "Installed";
      this.steamcmdInstallStatus = true;
    } else {
      this.steamInstalled = "Not Installed"
      this.steamcmdInstallStatus = false;
    }
  }

  buttonSteamInstallText(){
    if(this.steamInstalled == "Installed"){
      return "Force SteamCMD Update"
    } else {
      return "Install SteamCMD"
    }
  }
}
