import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
declare var Velocity: any;

@Component({
  selector: 'app-tick-animation',
  templateUrl: './tick-animation.component.html',
  styleUrls: ['./tick-animation.component.scss']
})
export class TickAnimationComponent implements OnInit {

  @ViewChild('tick') tick;
  @ViewChild('trigger') triggerDiv;
  @ViewChild('tickCircle') tickCircle;
  @ViewChild('tickP1') tickP1;
  @ViewChild('tickP2') tickP2;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
  }

  draw() {
    let that = this
    that.tick.nativeElement.style.display = "block";
  	Velocity(that.tickCircle.nativeElement, {strokeWidth: 30},200, "easeOutExpo")
    setTimeout(function() {
      Velocity(that.tickCircle.nativeElement, {strokeDasharray:1600,strokeDashoffset: 0},700, "easeInOutSine");
      setTimeout(function() {
        Velocity(that.tickP1.nativeElement, { height: 137.999, width: 30 },300, "easeInExpo");
        setTimeout(function() {
          Velocity(that.tickP2.nativeElement, { height: 259.334, width: 30 },500, "easeOutExpo" );
        }, 300)
      }, 400);
    }, 100)
  }

  erase() {
    this.tick.nativeElement.style.display = "none";
  }

}
