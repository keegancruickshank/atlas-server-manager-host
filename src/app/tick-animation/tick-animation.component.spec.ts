import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TickAnimationComponent } from './tick-animation.component';

describe('TickAnimationComponent', () => {
  let component: TickAnimationComponent;
  let fixture: ComponentFixture<TickAnimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TickAnimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TickAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
