import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { ServersComponent } from './pages/servers/servers.component';
import { AdminComponent } from './pages/admin/admin.component';
import { PlayersComponent } from './pages/players/players.component';

const routes: Routes = [
  {
    path: 'pages',
    component: PagesComponent,
    children: [
      {path: 'servers', component: ServersComponent},
      {path: 'admin', component: AdminComponent},
      {path: 'players', component: PlayersComponent}
    ]
  },
  { path: '',
    redirectTo: '/pages/servers',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
