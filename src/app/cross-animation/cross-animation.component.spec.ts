import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrossAnimationComponent } from './cross-animation.component';

describe('CrossAnimationComponent', () => {
  let component: CrossAnimationComponent;
  let fixture: ComponentFixture<CrossAnimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrossAnimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrossAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
