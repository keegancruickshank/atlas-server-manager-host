import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
declare var Velocity: any;

@Component({
  selector: 'app-cross-animation',
  templateUrl: './cross-animation.component.html',
  styleUrls: ['./cross-animation.component.scss']
})
export class CrossAnimationComponent implements OnInit {

  @ViewChild('cross') cross;
  @ViewChild('crossCircle') crossCircle;
  @ViewChild('crossP1') crossP1;
  @ViewChild('crossP2') crossP2;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
  }

  draw() {
    let that = this
    that.cross.nativeElement.style.display = "block";
  	Velocity(that.crossCircle.nativeElement, {strokeWidth: 30},200, "easeOutExpo")
    setTimeout(function() {
      Velocity(that.crossCircle.nativeElement, {strokeDasharray:1600,strokeDashoffset: 0},700, "easeInOutSine");
      setTimeout(function() {
        Velocity(that.crossP1.nativeElement, {height: 376.964, width: 33.205 },300, "easeOutQuart");
        setTimeout(function() {
          Velocity(that.crossP2.nativeElement, {height: 376.964, width: 33.205 },500, "easeOutQuart");
        }, 300)
      }, 400);
    }, 100)
  }

  erase() {
    this.cross.nativeElement.style.display = "none";
  }

}
