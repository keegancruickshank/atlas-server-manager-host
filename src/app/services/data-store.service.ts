import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
declare var electron: any;
declare var fs: any;
declare var path: any;

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {

  private path = "";
  private dataSubject = new BehaviorSubject(undefined);
  public dataObservable = this.dataSubject.asObservable();
  public data;
  public dataSet = false;

  public generateInitialSetup() {
    let that = this
    return new Promise((resolve, reject) => {
      const userDataPath = (electron.app || electron.remote.app).getPath('userData');
      that.path = path.join(userDataPath, 'config.json');
      that.data = that.parseDataFile(that.path);
      that.dataSubject.next(that.parseDataFile(that.path));
      resolve(that.data);
    })
  }

  constructor() {
    let that = this
    that.dataObservable.subscribe(data => {
      that.data = data
    })
  }

  public set(key, val) {
    this.data[key] = val;
    fs.writeFileSync(this.path, JSON.stringify(this.data));
    this.dataSubject.next(this.data)
  }

  public format() {
    fs.unlinkSync(this.path)
    this.data = undefined;
    this.dataSubject.next(this.data);
    this.dataSet = false;
  }

  private parseDataFile(filePath) {
    let that = this
    try {
      let json = JSON.parse(fs.readFileSync(filePath));
      return json
    } catch(error) {
      return {};
    }
  }
}
